import os
from common.ReadConfigIni import ReadConfigIni

#获取config目录真实路径
file_path = os.path.split(os.path.realpath(__file__))[0]
#print(file_path)

#获取项目真实路径
project_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
#print(project_path)

#######################
'''从这里切换测试环境'''
environment = "project"
#######################

#判断配置文件是否存在
if os.access(file_path,os.F_OK):

    # 读取配置文件
    read_config = ReadConfigIni(os.path.join(file_path,"config.ini"))

    #读配置文件的测试地址
    baseurl = read_config.getConfigValue(environment,"baseurl")
    #print(baseurl)

    #读配置文件的小程序影像云地址
    image_url = read_config.getConfigValue(environment,"image_url")

    # 读配置文件的小程序业务操作地址
    business_url = read_config.getConfigValue(environment,"business_url")

    #日志路径
    log_path = os.path.join(project_path,"logs")
    #print(log_path)

    #测试用例路径
    TestCase_path = os.path.join(project_path,"testcase")
    #print(TestCase_path)

    #报告路径
    report_path = os.path.join(project_path,"reports")
    #print(report_path)

    #测试数据路径
    data_path = os.path.join(project_path,"testdata")
    #print(data_path)

    #接口数据路径
    interface_data = os.path.join(data_path, "interface_data")

    #UI数据路径
    UI_data = os.path.join(data_path, "UI_data")
    #print(UI_data)

    #数据库地址
    DB_url = read_config.getConfigValue(environment,"DB_url")
    #print(DB_url)

    #数据库账号
    DB_user = read_config.getConfigValue(environment,"DB_user")
    #print(DB_user)

    # 数据库密码
    DB_pwd = read_config.getConfigValue(environment,"DB_pwd")
    #print(DB_pwd)

    # 数据库名
    DB = read_config.getConfigValue(environment,"DB")
    #print(DB)

    # 测试地址登陆名
    login_user = read_config.getConfigValue(environment,"login_user")

    # 测试地址登陆密码
    login_pwd = read_config.getConfigValue(environment,"login_pwd")
else:
    print('请检查配置文件是否存在！')



