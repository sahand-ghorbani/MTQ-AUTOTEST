from poium import Page, PageElement

#檬太奇后台-相册管理-相册搜索页面
class album_search(Page):
    #筛选功能
    album_number = PageElement(css='.ivu-form-item:nth-of-type(4) .ivu-input-default', describe="相册编号输入框")
    search_button = PageElement(css='.ivu-form-item:nth-of-type(7) span', describe="筛选按钮")
    #相册列表
    result = PageElement(css='.ivu-table-tbody td:nth-of-type(1) span', describe="相册编号")