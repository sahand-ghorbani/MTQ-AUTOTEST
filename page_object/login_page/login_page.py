from poium import Page, PageElement

#檬太奇后台-登录页面
class login_page(Page):
    username = PageElement(css='.ivu-form-item-required:nth-of-type(1) .ivu-input-default',  describe="用户名")
    password = PageElement(css='.ivu-form-item-required:nth-of-type(2) .ivu-input-default',  describe="密码")
    login_box = PageElement(css='.ivu-btn-primary',describe="登录按钮")