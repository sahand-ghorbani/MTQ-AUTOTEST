from poium import Page,PageElement

#檬太奇后台-动态管理-动态管理页面
class dynamic_managemnt(Page):
    #动态搜索功能
    nickname = PageElement(css='.ivu-form-item:nth-of-type(1) .ivu-input-default', describe="请输入用户昵称")
    sreach_btn = PageElement(css='.ivu-form-item:nth-of-type(4) span', describe="搜索按钮")
    result = PageElement(css='.ivu-tabs-tabpane:nth-of-type(1) tr:nth-of-type(1) td:nth-of-type(2) span', describe="搜索结果的用户名")

