
import os
import smtplib
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email import encoders
from email.header import Header
from config import globalconfig



#从配置文件读取报告路径
TestResult = globalconfig.report_path

def sendReport(file_new):
    with open(file_new,"rb") as f:
        mail_body = f.read()

        # 第三方 SMTP 服务
        mail_host = "smtp.qq.com"  # 设置服务器
        mail_user = "602700527@qq.com"  # 用户名
        mail_pass = "ajgnwehborklbbdc"  # 口令

        sender = '602700527@qq.com'
        receivers = ['602700527@qq.com']  # 接收邮件，可设置为你的QQ邮箱或者其他邮箱

        # 创建一个带附件的实例
        # related: 邮件内容的格式，采用内嵌的形式进行展示。
        message = MIMEMultipart()
        message['From'] = Header("自动化测试平台", 'utf-8')
        message['To'] = Header("", 'utf-8')
        subject = '自动化测试报告'
        message['Subject'] = Header(subject, 'utf-8')

        # 邮件正文内容
        message.attach(MIMEText('这是项目的自动化测试报告，详情请查看附件！', 'plain', 'utf-8'))

        # 构造附件1，传送报告目录下的 报告 文件
        att1 = MIMEText(open(file_new, 'rb').read(), "base64", "utf-8")
        att1["Content-Type"] = "application/octet-stream"
        att1["Content-Disposition"] = 'attachment; filename="report.html"'
        message.attach(att1)


        try:
            smtp = smtplib.SMTP()
            smtp.set_debuglevel(0)
            smtp.connect(mail_host)  # 邮箱服务器  （我这里用的时QQ的邮箱，其他邮箱需要在网上查一下！）
            smtp.login(mail_user, mail_pass)  # 登录邮箱
            smtp.sendmail(sender, receivers, message.as_string())  # 发送者和接收者
            smtp.quit()
            print("邮件已发出！注意查收。")
        except smtplib.SMTPException:
            print('发送失败了')

#排列找出最新的报告
def newReport(TestResult):

    lists = os.listdir(TestResult)
    # 获得最新的测试报告地址
    lists.sort()
    file_new = os.path.join(TestResult, lists[-1])
    #print(file_new)
    return file_new


# # # ######################################
# # # 验证是否可以发送文件，正式运行要注释掉或删除掉
# # # ######################################
if __name__ == "__main__":
    new_report = newReport(TestResult)
    sendReport(new_report)
