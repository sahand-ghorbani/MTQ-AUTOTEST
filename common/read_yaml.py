
import os
import yaml


class OperationYaml:


    def __init__(self,yamlPath):

        self.all_data = []

        self.load_file = self.getData(yamlPath)


    def load_yaml(self,yamlPath):


        os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

        BASE_DIR = os.path.dirname(os.path.realpath(__file__))
        #root_path = os.path.abspath(os.path.join(BASE_DIR, "../"))

        #full_path = root_path + yamlPath


        f = open(yamlPath, 'r', encoding = 'UTF-8')
        cfg = f.read()

        self.all_data = yaml.full_load(cfg)  # 用load方法转字典

        return self.all_data

    def getData(self,yamlPath):

        d = self.load_yaml(yamlPath)
        array = []  # 每条案例对应的测试数据信息
        desc = []   # 每条案例对应的描述信息
        for i in range(0,len(d)):
            for value in d[i].values():
                childarray = []
                #把读取到的url加入列表
                childarray.append(value['url'])
                #把读取到的send_data加入列表
                childarray.append(value['send_data'])
                #把读取到的except加入列表
                childarray.append(value['except'])
                print(value['url'])
                print(value['send_data'])
                print(value['except'])
                desc.append(value['desc'])
                array.append(tuple(childarray))

        #print(array)
        return array,desc



if __name__ == "__main__":
    test_data,case_desc=OperationYaml("D:\\UItest\\testdata\\login.yml").load_file

