import xlrd
import os,json
from config import globalconfig
data_path = globalconfig.data_path
#print(data_path)

# Sheet1
class Data_manipulation():
    '''操作数据文件'''

    def __init__(self,filename,sheetname):
        datapath = os.path.join(data_path,filename)
        self.workbook = xlrd.open_workbook(datapath)
        self.sheetName = self.workbook.sheet_by_name(sheetname)

    #读取excel
    def read_excel(self,rownum,colnum):
        value = self.sheetName.cell(rownum,colnum).value
        return value

    #写入excel
    def write_excel(self):
        pass

    # #保存数据到json文件
    # def save_json(self, data, file_path):
    #     '''传入要存储的数据和要存储的json文件路径'''
    #     # dumps 将数据转换成字符串
    #     json_str = json.dumps(data)
    #     # dump: 将数据写入json文件中
    #     with open(file_path, "w") as f:
    #         json.dump(json_str, f)
    #         print("文件写入完成...")
    #
    # #从json文件读取数据
    # def read_json(self, file_path):
    #     '''传入json'文件路径读取数据'''
    #     # 打开json文件读取数据
    #     with open(file_path, 'r') as load_f:
    #         # 字符串变换为数据类型
    #         data = json.load(load_f)
    #         # 返回读取的数据
    #         return data
    #
    # #保存数据到txt文件
    # def save_data(self, data, file_path):
    #     '''传入要存储的数据和要存储的文件路径'''
    #     f = open(file_path, 'w', encoding='utf-8')
    #     f.write(','.join(data))
    #     f.close()
    #
    # #从txt读取数据
    # def read_data(self, file_path):
    #     '''传入要读出的文件路径'''
    #     f = open(file_path, 'r', encoding='utf-8')
    #     data = f.read()
    #     print('从文件里面读出来的数据：', data)
    #     f.close()
    #     # 返回读取的数据
    #     return data

# # #########################################
# # 验证ReadExcel能够运行，正式运行要注释掉或删除掉##
# # #########################################
if __name__ == "__main__":
    cellValue = Data_manipulation("data.xlsx","Sheet1").read_excel(0,1)
    print(cellValue)