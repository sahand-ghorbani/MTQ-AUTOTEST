import json,os,sys

def save_json(self,data,file_path):
    '''传入要存储的数据和要存储的json文件路径'''
    # dumps 将数据转换成字符串
    json_str = json.dumps(data)
    #dump: 将数据写入json文件中
    with open(file_path, "w") as f:
        json.dump(json_str, f)
        print("文件写入完成...")

def read_json(self,file_path):
    '''传入json'文件路径读取数据'''
    #打开json文件读取数据
    with open(file_path, 'r') as load_f:
        #字符串变换为数据类型
        data = json.load(load_f)
        #返回读取的数据
        return data

def save_data(self,data,file_path):
    '''传入要存储的数据和要存储的文件路径'''
    f = open(file_path, 'w', encoding='utf-8')
    f.write(','.join(data))
    f.close()

def read_data(self,file_path):
    '''传入要读出的文件路径'''
    f = open(file_path, 'r', encoding='utf-8')
    data = f.read()
    print('从文件里面读出来的数据：', data)
    f.close()
    # 返回读取的数据
    return data