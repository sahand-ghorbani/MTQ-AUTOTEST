import xlrd
import os
from config import globalconfig
data_path = globalconfig.data_path
#print(data_path)

# Sheet1
class Excel():
    '''
    打开excel，获取测试数据
    '''
    def __init__(self,filename,sheetname):

        datapath = os.path.join(data_path,filename)
        self.workbook = xlrd.open_workbook(datapath)
        self.sheetName = self.workbook.sheet_by_name(sheetname)

    def read_excel(self,rownum,colnum):
        value = self.sheetName.cell(rownum,colnum).value
        return value

    def write_excel(self):
        pass

# # ######################################
# # 验证ReadExcel能够运行，正式运行要注释掉或删除掉
# # ######################################
if __name__ == "__main__":
    cellValue = Excel("data.xlsx","Sheet1").read_excel(0,1)
    print(cellValue)