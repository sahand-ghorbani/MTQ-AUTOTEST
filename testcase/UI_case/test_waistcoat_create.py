import pytest,os
from time import sleep
from operate.business_operate import businessoperate
from common.read_yaml import OperationYaml
from config import globalconfig
from page_object.waistcoat_mange.waistcoat_mange import waistcoat_mange
from page_object.waistcoat_mange.waistcoat_create.waistcoat_create import waistcoat_create
from common.random_number_generation import random_number
from common.upload_photo import upload_photo
from common.Database import search_db
from operate.browser_operate import BaseOperate

#从配置文件读取测试数据路径
datapath = globalconfig.UI_data
#yaml文件路径
yaml = os.path.join(datapath,'waistcoat_create.yml')

#定义参数名称
args_item = 'url,send_data,resultstr'

#读取yaml文件加载测试数据
test_data, case_desc = OperationYaml(yaml).load_file

@pytest.mark.parametrize(args_item, test_data, ids = case_desc)
class Test_waistcoat():

    @pytest.mark.waistcoat_create
    def test_waistcoat_create(self,browser,baseurl,url,send_data,resultstr):
        '''
        测试创建马甲
        '''
        # 登陆后台管理系统
        page = businessoperate().login_success(browser,baseurl)
        # 浏览器最大化
        browser.maximize_window()
        # 打开测试路径
        page.get(url)
        # 将浏览器驱动赋予测试页面
        page2 = waistcoat_mange(browser)
        # 点击创建马甲
        page2.create_button.click()
        # 将浏览器驱动赋予测试页面
        page3 = waistcoat_create(browser)
        # 选中用户类型
        BaseOperate().radio_buttons(page3.user_type)
        # 选中性别
        BaseOperate().radio_buttons(page3.sex)
        # 马甲昵称
        nickname = send_data['nickname'] + random_number()
        # 输入马甲昵称
        page3.nickname.send_keys(nickname)
        # 输入签名
        page3.signature.send_keys(send_data['signature'])
         # 点击上传头像
        page2.head_portrait.click()
        sleep(2)
        # 上传头像
        upload_photo()
        sleep(2)
        # 点击保存
        page2.save_button.click()
        sleep(2)
        # 查询数据库
        result = search_db().get_dbinfo(send_data['sql'],[nickname])
        results = result[0]
        # 用数据库结果比对断言
        assert nickname == results

if __name__ == '__main__':
    pytest.main(['-x','-v',r'D:\MTQ\testcase\UI_case\test_waistcoat_create.py'])
