import pytest,os
from time import sleep
from operate.business_operate import businessoperate
from common.read_yaml import OperationYaml
from config import globalconfig
from page_object.dynamic_management.dynamic_management import dynamic_managemnt

#从配置文件读取测试数据路径
datapath = globalconfig.UI_data
#拼接yaml文件路径
yaml = os.path.join(datapath,'dynamic_search.yml')

args_item = 'url,send_data,resultstr'

#读取yaml文件加载测试数据
test_data,case_desc = OperationYaml(yaml).load_file

@pytest.mark.parametrize(args_item, test_data, ids = case_desc)
class Test_dynamic():

    @pytest.mark.dynamic_search
    def test_dynamic_search(self,browser,baseurl,url,send_data,resultstr):
        '''
        测试动态搜索
        '''
        page = businessoperate().login_success(browser,baseurl)
        browser.maximize_window()
        page.get(url)
        sleep(2)
        page2 = dynamic_managemnt(browser)
        sleep(2)
        page2.nickname.send_keys(send_data['nickname'])
        sleep(2)
        page2.sreach_btn.click()
        sleep(2)
        result = page2.result.text
        assert result in resultstr['resultstr']
if __name__ == '__main__':
    pytest.main([r'D:\MTQ\testcase\UI_case\test_dynamic_search.py'])