import requests,pytest
from operate.interface_operate import interface_operate

class Test_upload_photo():

    @pytest.mark.upload_photo
    def test_upload_photo(self):
        '''小程序上传照片'''
        token,job_id,UploadUrl = interface_operate().get_info()

        filepath = r'D:\MTQ\testdata\girl.jpeg'
        f = open(filepath, 'rb')
        payload ={
                'access_token':'%s'%token,
                'job_id':'%s'%job_id,
                'length':'224488'
                }
        file = {'file': ('girl.jpeg',f,'image/jpeg')}

        r = requests.post(UploadUrl,data = payload,files = file)
        rsl = r.json()['code']
        assert 0 == rsl


if __name__ == '__main__':
    Test_upload_photo().test_upload_photo()