import requests,pytest
from operate.interface_operate import interface_operate
from config import globalconfig

image_url = globalconfig.image_url

class Test_login():

    @pytest.mark.login
    def test_login(self):
        '''小程序拿传图token'''

        payload = {
            'access_key': 'BCTyY2cdvcFZApwz',
            'store_id': 'C1509',
            'branch_id': 'C1509-1',
            'customer_name': '图片直播',
            'customer_phone': '13725372608',
            'scene_name': '时尚街拍',
            'product_sku': '1004576',
            'if_correct': '1',
            'channel_id': 'Beautify'
        }
        rsl = requests.post(image_url + '/api/fileapi/GetCAccessToken/', data=payload)
        r = rsl.json()['code']
        assert 0 == r

if __name__ == '__main__':
    Test_login().test_login()