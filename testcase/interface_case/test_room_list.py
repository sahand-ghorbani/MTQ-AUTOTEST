import requests,pytest
from config import globalconfig

baseurl = globalconfig.business_url

class Test_room_list():

    @pytest.mark.room_list
    def test_room_list(self):
        '''小程序相册列表'''

        url = baseurl + '/activityRoom/list'
        payload = {
            'unionId':'oYnHqs82GD8B6ceiztzIi3MkUbBU',
            'mobileNo':'15602518638',
            'pageNo':'1',
            'pageSize':'10'
        }

        r = requests.get(url,params = payload)
        rsl = r.json()['message']
        assert '操作成功' == rsl


if __name__ == '__main__':
    Test_room_list().test_room_list()