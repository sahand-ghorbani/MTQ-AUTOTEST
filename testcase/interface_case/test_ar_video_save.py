import requests,pytest,os
from config import globalconfig
from common.read_yaml import OperationYaml

# 从配置文件读取测试数据路径
datapath = globalconfig.interface_data

yaml = os.path.join(datapath,'ar_video_save.yml')

# 定义参数名称
args_item = 'url,send_data,resultstr'

# 读取yaml文件加载测试数据
test_data, case_desc = OperationYaml(yaml).load_file

@pytest.mark.parametrize(args_item,test_data,ids=case_desc)
class Test_ar_video_save():

    @pytest.mark.ar_video_save
    def test_ar_video_save(self,url,send_data,resultstr):
        '''檬太奇接受图形图像AR推送接口'''


        payload = send_data


        r = requests.post(url, data=payload)
        print(r.json())
        rsl = r.json()['code']
        assert rsl in resultstr['resultstr']

