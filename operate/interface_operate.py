import requests
from config import globalconfig

image_url = globalconfig.image_url
baseurl = globalconfig.business_url

'''接口业务操作封装'''
class interface_operate():
    def get_info(self):
        '''登录拿传图token等信息'''
        payload = {
            'access_key':'BCTyY2cdvcFZApwz',
            'store_id':'C1509',
            'branch_id':'C1509-1',
            'customer_name':'图片直播',
            'customer_phone':'13725372608',
            'scene_name':'时尚街拍',
            'product_sku':'1004576',
            'if_correct':'1',
            'channel_id':'Beautify'
        }
        reslt = requests.post(image_url+'/api/fileapi/GetCAccessToken/',data =payload)

        token = reslt.json()['data']['AccessToken']
        job_id = reslt.json()['data']['job_id']
        UploadUrl = reslt.json()['data']['UploadUrl']
        #print(UploadUrl)
        return token,job_id,UploadUrl

    def upload_photo(self):
        '''上传照片到相册'''
        token, job_id, UploadUrl = interface_operate().get_info()

        filepath = r'D:\MTQ\testdata\girl.jpeg'
        f = open(filepath, 'rb')
        payload = {
            'access_token': '%s' % token,
            'job_id': '%s' % job_id,
            'length': '224488'
        }
        file = {'file': ('girl.jpeg', f, 'image/jpeg')}

        r = requests.post(UploadUrl, data=payload, files=file)
        print(r.json())
        f.close()
        picID = r.json()['data']['FileName']
        #activityId = r.json()['data']['activityId']
        #return picID,activityId

    def upload_cfile(self):
        '''在创建动态中上传照片'''
        token, job_id, UploadUrl = interface_operate().get_info()
        url = 'https://upload2.hucai.com/api/fileapi/UploadCFile'
        filepath = r'D:\MTQ\testdata\girl.jpeg'
        f = open(filepath, 'rb')
        payload = {
            'access_token': '%s' % token,
            'job_id': '%s' % job_id,
            'length': '224488'
        }
        file = {'file': ('girl.jpeg', f, 'image/jpeg')}

        r = requests.post(url, data=payload, files=file)
        f.close()
        #print(r.json())
        picHeight = r.json()['data']['height']
        picId = r.json()['data']['FileName']
        picUrl = r.json()['data']['OssPath']
        picWidth = r.json()['data']['width']
        thumbnailUrl = r.json()['data']['oss_thumb_url']
        #print(picHeight)
        return picHeight,picId,picUrl,picWidth,thumbnailUrl

    def movement_create(self):
        '''创建动态拿ID号'''

        picHeight, picId, picUrl, picWidth, thumbnailUrl = interface_operate().upload_cfile()
        url = baseurl + '/movement/add'

        headers = {'content-type': 'application/json'}
        payload = {
            "content": "接口自动化测试创建动态",
            "imgUrlsList": [{
                "picHeight": "%s" % picHeight,
                "picId": "%s" % picId,
                "picUrl": "%s" % picUrl,
                "picWidth": "%s" % picWidth,
                "thumbnailUrl": "%s" % thumbnailUrl}],
            "tagsList": [],
            "unionId": "oYnHqs82GD8B6ceiztzIi3MkUbBU",
            "discoverId": "0"
        }

        r = requests.post(url=url, json=payload, headers=headers)
        print(r.json())
        id = r.json()['result']['id']
        return id

    def saveBrowingHistory(self):
        '''记录用户查看相册记录'''
        url = baseurl + '/activityRoom/saveBrowingHistory'

        payload = {
            'activityId':'hc-f-659357',
            'mobileNo':'17002299418',
            'unionId':'oYnHqszn5_jpN9aAgxCAQKYGYLIU'
        }

        r = requests.post(url,data=payload)
        print(r.json())


if __name__ == '__main__':
    interface_operate().saveBrowingHistory()
