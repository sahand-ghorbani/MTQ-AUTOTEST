# selenium_pytest 框架说明

##介绍：
  本框架为基于Python语言实现，测试数据，页面元素，测试用例互相独立，方便后期维护，报错信息清晰易读
  * 更改测试环境请修改config\globalconfig文件
  * 更改浏览器请修改contest.py的browser配置
###它包含功能:
  * 支持excel文件读写
  * 支持连接mysql数据库
  * 集成虫师poium库，元素维护更清晰
  * 支持运行失败自动截图
  * HTMLTestRunner生成规范的测试报告
  * 支持使用Pytest Marker标记运行特定测试用例集
  * 支持自动发送测试报告邮件
  * 支持与Jenkins集成，实现无人值守测试
  
  
Python版本与依赖库：
  * python3.7+ :https://www.python.org/

  * PyMySQL : https://github.com/PyMySQL/PyMySQL
    '''pip install pysysql'''
  * pytest-html
    '''pip install pytest-html'''
  * pytest
    '''pip   install  pytest'''
  * requests
    '''pip   install  requests'''
  * PyYAML
    '''pip install PyYAML'''
  * poium
    '''pip install poium'''
  * pytest-rerunfailures
    '''pip install pytest-rerunfailures'''
  * xlrd
    '''Pip install xlrd'''
  * allure
    '''pip install allure'''



##开发工具：
- PyCharm，下载地址：http://www.jetbrains.com/pycharm/download

---------

#### 工程目录结构及命名规范

地址：
```

```

##项目目录结构：

- XXXX/:  系统名称
  - common/： 公共函数。
  - config/： 配置文件
  - logs/： 运行日志。
  - operate/： 封装常用操作
  - page_object/：页面元素封装
  - reports/：存放测试报告
  - testcase/：测试用例
  - testdata/：测试数据
  - util/：存放杂项文件夹
  - run_test/:  执行入口


#####Commom
文件夹存放开发的公共方法；例如：读取yaml文件方法，读取mysql、oracle，上传附件等；

#####config
配置文件，系统URL/端口/邮件配置等配置信息；

#####logs
可配置收集测试日志

#####operate
封装常用的业务和浏览器操作

#####page_object
封装每个页面的元素，每个页面单独建立py文件，方便后期进行维护

#####Reports
存放测试报告

#####testcase
存放测试用例

#####testsuite
存放测试套件

#####util
存放其他杂项，例如测试中暂存的数据

#####run_test
批量执行测试用例的触发文件；

-------

##代码同步注意事项：
以下为框架自动生成的文件，请勿上传，避免造成代码冲突
######.pytest_cache文件夹
######testcase文件夹里的.pytest_cache文件夹
######reports文件夹
######geckodriver.log文件

 